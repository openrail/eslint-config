# Change Log for eslint-config

## [v1.4.0](https://gitlab.com/openrail/eslint-config-nodejs/tags/v1.4.0)
* fixed incorrect readme

## [v1.4.0](https://gitlab.com/openrail/eslint-config-nodejs/tags/v1.4.0)
* updated ESLint to v5
* added changelog (needs backfilling)
* added other comunity files