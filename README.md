# @openrail/eslint-config

[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://choosealicense.com/licenses/mit/)
[![npm (scoped)](https://img.shields.io/npm/v/@openrail/eslint-config.svg)](https://www.npmjs.com/package/@openrail/eslint-config)
[![Discord](https://img.shields.io/discord/478848557089030144.svg)](https://discord.gg/N9CPKaY)

A default ESLint configuration for use within OpenRail packages

## usage

run the following command to add this package to your devDependencies:
```bash
yarn add @openrail/eslint-config --dev
```

ontop of this you may have to install this packages peerDependencies which can be done by running the following command:
```bash
yarn add eslint eslint-config-airbnb-base eslint-plugin-import eslint-plugin-mocha eslint-plugin-node --dev 
```

and then create a `.eslint` file in the root of your project with the following contents:
```json
{
  "extends": "@openrail"
}
```